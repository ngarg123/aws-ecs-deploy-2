pytest==4.6.10
flake8==3.7.7
awscli==1.18.63

bitbucket-pipes-toolkit==1.14.0